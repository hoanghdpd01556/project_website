package edu.poly.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="LOAITIN")
public class Loaitin {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idloaitin")
	private int idloaitin;
	private String name;
	private String mota;
	private int giatien;
	@OneToMany(mappedBy="loaitinType",fetch=FetchType.EAGER)
	
	public String getName() {
		return name;
	}
	public int getGiatien() {
		return giatien;
	}
	public void setGiatien(int giatien) {
		this.giatien = giatien;
	}
	public int getIdloaitin() {
		return idloaitin;
	}
	public void setIdloaitin(int idloaitin) {
		this.idloaitin = idloaitin;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	
	
}
