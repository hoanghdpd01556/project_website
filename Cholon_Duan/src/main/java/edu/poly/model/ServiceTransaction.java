package edu.poly.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name = "GIAODICH")
public class ServiceTransaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Double sotiengiaodich;
	@DateTimeFormat(pattern = "dd/mm/yyyy HH:mm:ss")
	private Date ngaygiaodich;
	private String mota;
	@ManyToOne
	@JoinColumn(name = "tentaikhoan")
	User usertransaction;
	@ManyToOne
	@JoinColumn(name = "maloaigiaodich")
	ServiceMoney serviceMoney;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Date getNgaygiaodich() {
		return ngaygiaodich;
	}
	public void setNgaygiaodich(Date ngaygiaodich) {
		this.ngaygiaodich = ngaygiaodich;
	}
	public User getUsertransaction() {
		return usertransaction;
	}
	public void setUsertransaction(User usertransaction) {
		this.usertransaction = usertransaction;
	}
	public ServiceMoney getServiceMoney() {
		return serviceMoney;
	}
	public void setServiceMoney(ServiceMoney serviceMoney) {
		this.serviceMoney = serviceMoney;
	}
	public Double getSotiengiaodich() {
		return sotiengiaodich;
	}
	public void setSotiengiaodich(Double sotiengiaodich) {
		this.sotiengiaodich = sotiengiaodich;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	
}
