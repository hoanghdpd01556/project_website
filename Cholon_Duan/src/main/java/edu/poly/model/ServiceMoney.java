package edu.poly.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LOAIGIAODICH")
public class ServiceMoney {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idloaigiaodich;
	private String tengiaodich;
	private String mota;
	@OneToMany(mappedBy="usertransaction",fetch=FetchType.EAGER)
	public int getIdloaigiaodich() {
		return idloaigiaodich;
	}

	public void setIdloaigiaodich(int idloaigiaodich) {
		this.idloaigiaodich = idloaigiaodich;
	}

	public String getTengiaodich() {
		return tengiaodich;
	}

	public void setTengiaodich(String tengiaodich) {
		this.tengiaodich = tengiaodich;
	}

	public String getMota() {
		return mota;
	}

	public void setMota(String mota) {
		this.mota = mota;
	}

}
