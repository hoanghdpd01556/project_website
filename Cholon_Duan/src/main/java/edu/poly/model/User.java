package edu.poly.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "TAIKHOAN")
public class User {
	@Id
	private String tentaikhoan;
	private String matkhau;
	private String hinhanh;
	private String hoten;
	private String sdt;
	private String diachi;
	private int vaitro;
	private int bitcoint;
	private Date ngaythamgia;
	@OneToMany(mappedBy = "usertransaction", fetch = FetchType.EAGER)
	public String getTentaikhoan() {
		return tentaikhoan;
	}
	public Date getNgaythamgia() {
		return ngaythamgia;
	}

	public void setNgaythamgia(Date ngaythamgia) {
		this.ngaythamgia = ngaythamgia;
	}

	@OneToMany(mappedBy = "userpost", fetch = FetchType.EAGER)
	public void setTentaikhoan(String tentaikhoan) {
		this.tentaikhoan = tentaikhoan;
	}

	public String getMatkhau() {
		return matkhau;
	}

	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public String getDiachi() {
		return diachi;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public int getVaitro() {
		return vaitro;
	}

	public void setVaitro(int vaitro) {
		this.vaitro = vaitro;
	}

	public int getBitcoint() {
		return bitcoint;
	}

	public void setBitcoint(int d) {
		this.bitcoint = d;
	}

	public String getHinhanh() {
		return hinhanh;
	}

	public void setHinhanh(String hinhanh) {
		this.hinhanh = hinhanh;
	}

}
