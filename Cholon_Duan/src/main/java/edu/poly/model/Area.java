package edu.poly.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="KHUVUC")
public class Area {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idkhuvuc")
	private int idkhuvuc;
	private String tenkhuvuc;
	@OneToMany(mappedBy="areaType",fetch=FetchType.EAGER)
	public int getIdkhuvuc() {
		return idkhuvuc;
	}
	public void setIdkhuvuc(int idkhuvuc) {
		this.idkhuvuc = idkhuvuc;
	}
	public String getTenkhuvuc() {
		return tenkhuvuc;
	}
	public void setTenkhuvuc(String tenkhuvuc) {
		this.tenkhuvuc = tenkhuvuc;
	}
	
}
