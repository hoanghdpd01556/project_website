package edu.poly.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BAIDANG")
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String tieude;
	private String anh;
	private String noidung;
	private Double price;
	private Date ngaydang;
	public Date getNgaydang() {
		return ngaydang;
	}
	public void setNgaydang(Date ngaydang) {
		this.ngaydang = ngaydang;
	}
	@ManyToOne
	@JoinColumn(name = "iddanhmuc")
	Category categoryType;
	@ManyToOne
	@JoinColumn(name = "tentaikhoan")
	User userpost;
	@ManyToOne
	@JoinColumn(name = "idkhuvuc")
	Area areaType;
	@ManyToOne
	@JoinColumn(name = "idloaitin")
	Loaitin loaitinType;

	public Loaitin getLoaitinType() {
		return loaitinType;
	}

	public void setLoaitinType(Loaitin loaitinType) {
		this.loaitinType = loaitinType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Category getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Category categoryType) {
		this.categoryType = categoryType;
	}

	public String getTieude() {
		return tieude;
	}

	public void setTieude(String tieude) {
		this.tieude = tieude;
	}

	public String getAnh() {
		return anh;
	}

	public void setAnh(String anh) {
		this.anh = anh;
	}

	public String getNoidung() {
		return noidung;
	}

	public void setNoidung(String noidung) {
		this.noidung = noidung;
	}

	public Area getAreaType() {
		return areaType;
	}

	public void setAreaType(Area areaType) {
		this.areaType = areaType;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public User getUserpost() {
		return userpost;
	}

	public void setUserpost(User userpost) {
		this.userpost = userpost;
	}

}
