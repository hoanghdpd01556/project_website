package edu.poly.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import edu.poly.model.User;
import edu.poly.service.IUserService;

public class UserAction extends ActionSupport implements SessionAware, ModelDriven<User>, Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4;
	private Map<String, Object> sessionAtributes = null;
	private User user = new User();
	private List<User> users = null;
	private String id = null;
	private IUserService userService;

	public UserAction(IUserService userService) {
		this.userService = userService;
	}

	public String add() {
		if (user != null && user.getTentaikhoan() != null) {
			user.setNgaythamgia(new Date());
			userService.saveOrUpdate(user);

			return execute();
		}
		return INPUT;
	}

	public String edit() {
		if (id == null) {
			return execute();
		} else if (id != null && user != null && user.getTentaikhoan() != null) {
			userService.saveOrUpdate(user);
		} else {
			user = userService.get(id);
		}
		return INPUT;
	}

	public String delete() {
		if (id != null) {
			userService.delete(id);
			addActionMessage(String.format("The user %s has been deleted", id));
		}
		return execute();
	}

	public String execute() {
		users = userService.list();
		return SUCCESS;
	}

	public String profile() {
		sessionAtributes = ActionContext.getContext().getSession();
		user = (User) sessionAtributes.get("USER");
		return SUCCESS;
	}

	public String search() {
		if (user != null && user.getHoten() != null) {
			users = userService.search(user.getHoten());
		}
		return SUCCESS;
	}

	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		if (id != null)
			user = userService.get(id);
	}

	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub
		this.sessionAtributes = session;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getSessionAtributes() {
		return sessionAtributes;
	}

	public void setSessionAtributes(Map<String, Object> sessionAtributes) {
		this.sessionAtributes = sessionAtributes;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
