package edu.poly.action;

import java.util.List;
import java.util.Map;


import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import edu.poly.model.User;
import edu.poly.service.IUserService;

public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3814140055808453874L;
	private User user = new User();

	@Autowired
	private IUserService service;
	private Map<String, Object> sesstionAttributes = null;
	private List<User> users = null;
	private String id = null;
	private String userr = null;
	private String message = null;
	public LoginAction(IUserService service) {
		this.service = service;
	}

	public String getUserr() {
		return userr;
	}

	public void setUserr(String userr) {
		this.userr = userr;
	}
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String execute() {
		if (user.getTentaikhoan()==null || user.getTentaikhoan().equals("") || user.getMatkhau()== null || user.getMatkhau().equals("")) {
			if(user.getTentaikhoan()==null || user.getTentaikhoan().equals("")) {
				addFieldError("taikhoan", "Vui lòng nhập tài khoản");
			}
			if(user.getMatkhau()== null || user.getMatkhau().equals("")) {
				addFieldError("errormatkhau", "Vui lòng nhập mật khẩu");
			}
			return INPUT;
		}
		user = service.checkLogin(user.getTentaikhoan(), user.getMatkhau());
		if (user != null && user.getVaitro() == 0) {
			sesstionAttributes.put("USER", user);
			users = service.list();
			return SUCCESS;
		} else if (user != null && user.getVaitro() == 1) {
			sesstionAttributes.put("USER", user);
			return "welcome";
		}else {
			addActionError("Tài khoản hoặc mật khẩu không đúng");
			return INPUT;
		}
		
	}
	public String logout() {
		sesstionAttributes.remove("USER");
		return SUCCESS;
	}
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	public void setSession(Map<String, Object> sessionAttributes) {
		this.sesstionAttributes = sessionAttributes;

	}

	public User getUser() {
		// TODO Auto-generated method stub
		return user;
	}

	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user;
	}

	public IUserService getService() {
		return service;
	}

	public void setService(IUserService service) {
		this.service = service;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getSesstionAttributes() {
		return sesstionAttributes;
	}

	public void setSesstionAttributes(Map<String, Object> sesstionAttributes) {
		this.sesstionAttributes = sesstionAttributes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
