package edu.poly.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import edu.poly.model.User;
import edu.poly.service.IUserService;

public class DashbroadAction extends ActionSupport implements SessionAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sessionAtributes = null;
	private User user = new User();
	private List<User> users = null;
	private IUserService userService;
	private int total = 0;
	private String lala = "kaka";
	@SuppressWarnings("deprecation")
	private Date ngayhethong = new Date("yyyy-MM-dd");

	public Date getNgayhethong() {
		return ngayhethong;
	}
	
	public String getLala() {
		return lala;
	}

	public void setLala(String lala) {
		this.lala = lala;
	}

	public void setNgayhethong(Date ngayhethong) {
		this.ngayhethong = ngayhethong;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Map<String, Object> getSessionAtributes() {
		return sessionAtributes;
	}

	public void setSessionAtributes(Map<String, Object> sessionAtributes) {
		this.sessionAtributes = sessionAtributes;
	}
	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub
		this.sessionAtributes = session;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String execute() {
		lala = "abc";
		return SUCCESS;
	}
}
