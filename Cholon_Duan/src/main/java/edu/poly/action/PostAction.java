package edu.poly.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import edu.poly.model.Area;
import edu.poly.model.Category;
import edu.poly.model.Loaitin;
import edu.poly.model.Post;
import edu.poly.model.User;
import edu.poly.service.IPostService;
import edu.poly.service.IUserService;

public class PostAction extends ActionSupport implements SessionAware, ModelDriven<Post>, Preparable {
	private static final long serialVersionUID = 4;
	private Map<String, Object> sessionAtributes = null;
	private Post post = new Post();
	private List<Post> posts = null;
	private List<Area> areatype = null;
	private List<Category> categorytype = null;
	private List<Loaitin> loaitintype = null;
	private String id = null;
	private int khuvuc, danhmuc;
	private IPostService postService;
	private User user;
	private IUserService userService;
	private String tukhoa = null;

	public PostAction(IPostService postService, IUserService userService) {
		this.postService = postService;
		this.userService = userService;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public void setPostService(IPostService postService) {
		this.postService = postService;
	}

	public User getUser() {
		return user;
	}

	public String getTukhoa() {
		return tukhoa;
	}

	public void setTukhoa(String tukhoa) {
		this.tukhoa = tukhoa;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getKhuvuc() {
		return khuvuc;
	}

	public void setKhuvuc(int khuvuc) {
		this.khuvuc = khuvuc;
	}

	public int getDanhmuc() {
		return danhmuc;
	}

	public void setDanhmuc(int danhmuc) {
		this.danhmuc = danhmuc;
	}

	public List<Area> getAreatype() {
		return areatype;
	}

	public void setAreatype(List<Area> areatype) {
		this.areatype = areatype;
	}

	public List<Category> getCategorytype() {
		return categorytype;
	}

	public void setCategorytype(List<Category> categorytype) {
		this.categorytype = categorytype;
	}

	public String add() {
		areatype = postService.listarea();
		categorytype = postService.listcategory();
		loaitintype = postService.listloaitin();
		user = new User();
		post.setNgaydang(new Date());
		if (post != null && post.getAreaType() != null && post.getCategoryType() != null) {
			sessionAtributes = ActionContext.getContext().getSession();
			post.setNgaydang(new Date());
			user = (User) sessionAtributes.get("USER");
			post.setUserpost(user);
			postService.saveOrUpdate(post);

			return execute();
		}
		return INPUT;
	}

	public String addMember() {
		sessionAtributes = ActionContext.getContext().getSession();
		areatype = postService.listarea();
		categorytype = postService.listcategory();
		loaitintype = postService.listloaitin();
		user = new User();
		user = (User) sessionAtributes.get("USER");
		post.setNgaydang(new Date());
		if (post != null && post.getAreaType() != null && post.getCategoryType() != null
				&& post.getLoaitinType() != null) {
			sessionAtributes = ActionContext.getContext().getSession();
			if (post.getLoaitinType().getIdloaitin() > 1) {
				if (user.getBitcoint() >= loaitintype.get(1).getGiatien()) {
					int amount = user.getBitcoint();
					for (int i = 0; i < loaitintype.size(); i++) {
						if (loaitintype.get(i).getIdloaitin() == post.getLoaitinType().getIdloaitin()) {
							amount -= loaitintype.get(i).getGiatien();
						}
					}
					user.setBitcoint(amount);
					userService.saveOrUpdate(user);
				} else {
					addActionMessage("Khong du tien");
					return INPUT;
				}
			}
			post.setNgaydang(new Date());
			post.setUserpost(user);
			postService.saveOrUpdate(post);
			return execute();
		} else {
			return INPUT;
		}

	}

	public String edit() {
		if (id == null) {
			return execute();
		} else if (id != null && post != null && post.getUserpost() != null) {
			postService.saveOrUpdate(post);
		} else {
			post = postService.get(id);
		}
		return INPUT;
	}

	public String delete() {
		if (id != null) {
			postService.delete(Integer.parseInt(id));
			addActionMessage(String.format("The user %s has been deleted", id));
		}
		return execute();
	}

	public String profile() {
		sessionAtributes = ActionContext.getContext().getSession();
		user = new User();
		user = (User) sessionAtributes.get("USER");
		posts = postService.listgetID(user.getTentaikhoan());
		return SUCCESS;
	}

	public String execute() {
		areatype = postService.listarea();
		categorytype = postService.listcategory();
		loaitintype = postService.listloaitin();
		user = new User();
		user = (User) sessionAtributes.get("USER");
		return SUCCESS;
	}

	public String searchM() {
		posts = postService.timkiem(khuvuc, danhmuc, tukhoa);
		return SUCCESS;
	}

	public String search() {
		posts = postService.search(khuvuc, danhmuc);

		return execute();

	}

	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		if (id != null)
			post = postService.get(id);
	}

	public Post getModel() {
		// TODO Auto-generated method stub
		return post;
	}

	public Map<String, Object> getSessionAtributes() {
		return sessionAtributes;
	}

	public void setSessionAtributes(Map<String, Object> sessionAtributes) {
		this.sessionAtributes = sessionAtributes;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public IPostService getPostService() {
		return postService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setSession(Map<String, Object> sessionAtributes) {
		// TODO Auto-generated method stub
		this.sessionAtributes = sessionAtributes;
	}

	public List<Loaitin> getLoaitintype() {
		return loaitintype;
	}

	public void setLoaitintype(List<Loaitin> loaitintype) {
		this.loaitintype = loaitintype;
	}

}
