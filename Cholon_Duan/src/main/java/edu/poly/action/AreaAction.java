package edu.poly.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import edu.poly.model.Area;
import edu.poly.service.IAreaService;

public class AreaAction  extends ActionSupport implements SessionAware, ModelDriven<Area>, Preparable {
	private static final long serialVersionUID = 4;
	private Map<String, Object> sessionAtributes = null;
	private Area area = new Area();
	private List<Area> areas = null;
	private String id = null;
	private IAreaService areaService;
	public AreaAction(IAreaService areaService) {
		this.areaService = areaService;
	}
	public String add() {
		if (area != null) {
			sessionAtributes = ActionContext.getContext().getSession();
			areaService.saveOrUpdate(area);
			return execute();
		}
		return INPUT;
	}

	public String edit() {
		if (id == null) {
			return execute();
		} else if (id != null && area != null) {
			areaService.saveOrUpdate(area);
		} else {
			area = areaService.get(Integer.parseInt(id));
		}
		return INPUT;
	}

	public String delete() {
		if (id != null) {
			areaService.delete(Integer.parseInt(id));
			addActionMessage(String.format("The user %s has been deleted", id));
		}
		return execute();
	}

	public String execute() {
			areas = areaService.list();
			return SUCCESS;
		
	}

	public String search() {
		if (area != null && area.getIdkhuvuc()!= 0) {
			areas = areaService.search(area.getIdkhuvuc());
		}
		return SUCCESS;
	}

	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		if (id != null)
			area = areaService.get(Integer.parseInt(id));
	}
	public Area getModel() {
		// TODO Auto-generated method stub
		return area;
	}
	public Map<String, Object> getSessionAtributes() {
		return sessionAtributes;
	}
	public void setSessionAtributes(Map<String, Object> sessionAtributes) {
		this.sessionAtributes = sessionAtributes;
	}
	
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public List<Area> getAreas() {
		return areas;
	}
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public IAreaService getAreaService() {
		return areaService;
	}
	public void setAreaService(IAreaService areaService) {
		this.areaService = areaService;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setSession(Map<String, Object> sessionAtributes) {
		// TODO Auto-generated method stub
		this.sessionAtributes = sessionAtributes;
	}
	
}
