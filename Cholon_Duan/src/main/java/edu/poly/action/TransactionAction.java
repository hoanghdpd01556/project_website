package edu.poly.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import edu.poly.model.ServiceTransaction;
import edu.poly.model.User;
import edu.poly.service.ITransactionService;;

public class TransactionAction extends ActionSupport
		implements SessionAware, ModelDriven<ServiceTransaction>, Preparable {
	private ServiceTransaction serviceTransaction = new ServiceTransaction();
	private List<ServiceTransaction> transactions = null;
	private static final long serialVersionUID = 4;
	private String id = null;
	private Map<String, Object> sessionAtributes = null;
	private ITransactionService iTransactionService;
	private User user = new User();

	public TransactionAction(ITransactionService transactionService) {
		this.iTransactionService = transactionService;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String execute() {
		sessionAtributes = ActionContext.getContext().getSession();
		user = (User) sessionAtributes.get("USER");
		transactions = iTransactionService.listgetID(user.getTentaikhoan());
		return SUCCESS;
	}

	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

	public ServiceTransaction getModel() {
		// TODO Auto-generated method stub

		return serviceTransaction;
	}

	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub

	}

	public ServiceTransaction getServiceTransaction() {
		return serviceTransaction;
	}

	public void setServiceTransaction(ServiceTransaction serviceTransaction) {
		this.serviceTransaction = serviceTransaction;
	}

	public Map<String, Object> getSessionAtributes() {
		return sessionAtributes;
	}

	public void setSessionAtributes(Map<String, Object> sessionAtributes) {
		this.sessionAtributes = sessionAtributes;
	}

	public ITransactionService getiTransactionService() {
		return iTransactionService;
	}

	public void setiTransactionService(ITransactionService iTransactionService) {
		this.iTransactionService = iTransactionService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ServiceTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<ServiceTransaction> transactions) {
		this.transactions = transactions;
	}

}
