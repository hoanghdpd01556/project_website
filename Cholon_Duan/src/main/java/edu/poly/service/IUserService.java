package edu.poly.service;

import java.util.Date;
import java.util.List;

import edu.poly.model.User;

public interface IUserService {
public List<User> list();
public void saveOrUpdate(User user);
public void delete(String id);
public User get(String username);
public User checkLogin(String username,String passwords);
public List<User> search(String name);
public void savebitcoint(User user);
public List<User> total(Date ngayhomnay);
}
