package edu.poly.service;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

import edu.poly.model.Area;
import edu.poly.model.Post;
import edu.poly.model.User;

public class AreaService implements IAreaService {
	private SessionFactory sessionFactory;

	public AreaService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Area> list() {
		@SuppressWarnings({ "deprecation", "unused" })
		List<Area> listpost = (List<Area>) sessionFactory.getCurrentSession().createCriteria(Area.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listpost;
	}

	@Transactional
	public void saveOrUpdate(Area area) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(area);

	}

	@Transactional
	public void delete(int idkhuvuc) {
		// TODO Auto-generated method stub
		Area postToDelete = new Area();
		postToDelete.setIdkhuvuc(idkhuvuc);
		sessionFactory.getCurrentSession().delete(postToDelete);

	}

	@Transactional
	public Area get(int id) {
		// TODO Auto-generated method stub
		Area area = sessionFactory.getCurrentSession().find(Area.class, id);
		return area;
	}

	public List<Area> search(int aa) {
		// TODO Auto-generated method stub
		String hql = "from Area where tenkhuvuc like:aa";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Area> listPost = (List<Area>) query.getResultList();
		return listPost;
	}

}
