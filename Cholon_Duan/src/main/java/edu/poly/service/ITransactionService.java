package edu.poly.service;

import java.util.List;

import edu.poly.model.ServiceTransaction;;

public interface ITransactionService {
	public List<ServiceTransaction> list();
	public List<ServiceTransaction> listgetID(String tentk);
	public void saveOrUpdate(ServiceTransaction serviceTransaction);
	public void delete(int id);
	public ServiceTransaction get(String username);
}
