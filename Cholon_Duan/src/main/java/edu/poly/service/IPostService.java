package edu.poly.service;

import java.util.List;

import edu.poly.model.Area;
import edu.poly.model.Category;
import edu.poly.model.Loaitin;
import edu.poly.model.Post;

public interface IPostService {
	public List<Post> list();
	public List<Post> listgetID(String tentk);
	public void saveOrUpdate(Post post);
	public void delete(int id);
	public Post get(String username);
	public List<Post> search(int khuvuc,int danhmuc);
	public List<Area> listarea();
	public List<Category> listcategory();
	public List<Loaitin> listloaitin();
	public List<Post> timkiem(int khuvuc,int danhmuc,String tukhoa);
}
