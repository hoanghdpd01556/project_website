package edu.poly.service;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

import edu.poly.model.Area;
import edu.poly.model.Category;
import edu.poly.model.Post;
import edu.poly.model.User;

public class CategoryService implements ICategoryService{
	private SessionFactory sessionFactory;

	public CategoryService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Category> list() {
		@SuppressWarnings({ "deprecation", "unused" })
		List<Category> listpost = (List<Category>) sessionFactory.getCurrentSession().createCriteria(Category.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listpost;
	}
	@Transactional
	public void saveOrUpdate(Category category) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(category);
		
	}
	@Transactional
	public void delete(int iddanhmuc) {
		// TODO Auto-generated method stub
		Category postToDelete = new Category();
		postToDelete.setIddanhmuc(iddanhmuc);
		sessionFactory.getCurrentSession().delete(postToDelete);
		
	}
	@Transactional
	public Category get(int id) {
		// TODO Auto-generated method stub
		Category category = sessionFactory.getCurrentSession().find(Category.class, id);
		return category;
	}

	public List<Category> search(int aa) {
		// TODO Auto-generated method stub
		String hql = "from Category where tenkhuvuc like:aa";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Category> listPost = (List<Category>) query.getResultList();
		return listPost;
	}

}
