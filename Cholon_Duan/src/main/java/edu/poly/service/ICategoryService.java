package edu.poly.service;

import java.util.List;

import edu.poly.model.Area;
import edu.poly.model.Category;

public interface ICategoryService {
	public List<Category> list();
	public void saveOrUpdate(Category category);
	public void delete(int id);
	public Category get(int id);
	public List<Category> search(int name);
}
