package edu.poly.service;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import edu.poly.model.Post;
import edu.poly.model.User;

public class UserService implements IUserService {
	private SessionFactory sessionFactory;

	public UserService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<User> list() {
		// TODO Auto-generated method stub
		@SuppressWarnings({ "deprecation", "unused" })
		List<User> listuser = (List<User>) sessionFactory.getCurrentSession().createCriteria(User.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listuser;
	}

	@Transactional
	public void saveOrUpdate(User user) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(user);

	}

	public void delete(String username) {
		// TODO Auto-generated method stub
		User userToDelete = new User();
		userToDelete.setTentaikhoan(username);
		sessionFactory.getCurrentSession().delete(userToDelete);
	}

	@Transactional
	public User get(String username) {
		// TODO Auto-generated method stub
		User user = sessionFactory.getCurrentSession().find(User.class, username);
		return user;
	}

	@Transactional
	public User checkLogin(String username, String matkhau) {
		// TODO Auto-generated method stub
		User user = get(username);
		if (user != null && user.getMatkhau().equals(matkhau)) {
			return user;
		}
		return null;
	}
	@Transactional
	public List<User> search(String name) {
		// TODO Auto-generated method stub
		String hql = "from User where name like:name";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<User> listUser = (List<User>) query.getResultList();
		return listUser;
	}

	public void savebitcoint(User user) {
		// TODO Auto-generated method stub

	}
	@SuppressWarnings("deprecation")
	@Transactional
	public List<User> total(Date ngayhomnay) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.like("ngaydang", "2017-12-07%")).list();
		
		return users;
	}

}
