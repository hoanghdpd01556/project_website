package edu.poly.service;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import edu.poly.model.Area;
import edu.poly.model.Category;
import edu.poly.model.Loaitin;
import edu.poly.model.Post;

public class PostService implements IPostService {
	private SessionFactory sessionFactory;

	public PostService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Post> list() {
		@SuppressWarnings({ "deprecation" })
		List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Property.forName("id").desc()).list();
		return listpost;
	}

	@Transactional
	public void saveOrUpdate(Post post) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(post);

	}

	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Post postToDelete = new Post();
		postToDelete.setId(id);
		sessionFactory.getCurrentSession().delete(postToDelete);

	}

	@Transactional
	public Post get(String username) {
		// TODO Auto-generated method stub
		Post post = sessionFactory.getCurrentSession().find(Post.class, username);
		return post;
	}

	@Transactional
	public List<Area> listarea() {
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<Area> listarea = (List<Area>) sessionFactory.getCurrentSession().createCriteria(Area.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listarea;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public List<Category> listcategory() {
		// TODO Auto-generated method stub
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<Category> listcategory = (List<Category>) sessionFactory.getCurrentSession().createCriteria(Category.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listcategory;
	}

	@Transactional
	public List<Post> search(int khuvuc, int danhmuc) {
		// TODO Auto-generated method stub
		String hql;
		if (khuvuc == -1 && danhmuc != -1) {
			hql = "from Post where iddanhmuc = :danhmuc";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("danhmuc", danhmuc);
			@SuppressWarnings("unchecked")
			List<Post> listPost = (List<Post>) query.getResultList();
			return listPost;
		} else if (danhmuc == -1 && khuvuc != -1) {
			hql = "from Post where idkhuvuc = :khuvuc";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("khuvuc", khuvuc);
			@SuppressWarnings("unchecked")
			List<Post> listPost = (List<Post>) query.getResultList();
			return listPost;

		} else {
			hql = "from Post where idkhuvuc = :khuvuc and iddanhmuc = :danhmuc";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("khuvuc", khuvuc);
			query.setParameter("danhmuc", danhmuc);
			@SuppressWarnings("unchecked")
			List<Post> listPost = (List<Post>) query.getResultList();
			return listPost;
		}

	}

	@Transactional
	public List<Loaitin> listloaitin() {
		// TODO Auto-generated method stub
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<Loaitin> listloaitin = (List<Loaitin>) sessionFactory.getCurrentSession().createCriteria(Loaitin.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return listloaitin;
	}

	@Transactional
	public List<Post> listgetID(String tentk) {
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
				.add(Restrictions.like("userpost.tentaikhoan", tentk)).addOrder(Property.forName("id").desc()).list();
		return listpost;
	}
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional
	public List<Post> timkiem(int khuvuc, int danhmuc, String tukhoa) {
		if (tukhoa.isEmpty()) {
			if (khuvuc == 0 && danhmuc != 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("categoryType.iddanhmuc", danhmuc))
						.addOrder(Property.forName("id").desc()).list();
				return listpost;
			} else if (khuvuc != 0 && danhmuc == 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("areaType.idkhuvuc", khuvuc)).addOrder(Property.forName("id").desc())
						.list();
				return listpost;
			} else if (khuvuc != 0 && danhmuc != 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("areaType.idkhuvuc", khuvuc))
						.add(Restrictions.like("categoryType.iddanhmuc", danhmuc))
						.addOrder(Property.forName("id").desc()).list();
				return listpost;
			} else {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.addOrder(Property.forName("id").desc()).list();
				return listpost;
			}
		} else {
			if (khuvuc == 0 && danhmuc != 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("categoryType.iddanhmuc", danhmuc))
						.add(Restrictions.like("tieude", "%" + tukhoa + "%")).addOrder(Property.forName("id").desc())
						.list();
				return listpost;
			} else if (khuvuc != 0 && danhmuc == 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("areaType.idkhuvuc", khuvuc))
						.add(Restrictions.like("tieude", "%" + tukhoa + "%")).addOrder(Property.forName("id").desc())
						.list();
				return listpost;
			} else if (khuvuc != 0 && danhmuc != 0) {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("areaType.idkhuvuc", khuvuc))
						.add(Restrictions.like("categoryType.iddanhmuc", danhmuc))
						.add(Restrictions.like("tieude", "%" + tukhoa + "%")).addOrder(Property.forName("id").desc())
						.list();
				return listpost;
			} else {
				List<Post> listpost = (List<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
						.add(Restrictions.like("tieude", "%" + tukhoa + "%")).addOrder(Property.forName("id").desc())
						.list();
				return listpost;
			}
		}

	}

}
