package edu.poly.service;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import edu.poly.model.Post;
import edu.poly.model.ServiceTransaction;

public class TransactionService implements ITransactionService{
	private SessionFactory sessionFactory;

	public TransactionService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public List<ServiceTransaction> list() {
		// TODO Auto-generated method stub
		return null;
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ServiceTransaction> listgetID(String tentk) {
		// TODO Auto-generated method stub
		List<ServiceTransaction> list = (List<ServiceTransaction>) sessionFactory.getCurrentSession().createCriteria(ServiceTransaction.class).add(Restrictions.like("usertransaction.tentaikhoan", tentk)).addOrder(Property.forName("id").desc()).list();
		return list;
	}

	public void saveOrUpdate(ServiceTransaction serviceTransaction) {
		// TODO Auto-generated method stub
		
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	public ServiceTransaction get(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
