package edu.poly.service;

import java.util.List;

import edu.poly.model.Area;

public interface IAreaService {
	public List<Area> list();
	public void saveOrUpdate(Area area);
	public void delete(int id);
	public Area get(int id);
	public List<Area> search(int name);
}
