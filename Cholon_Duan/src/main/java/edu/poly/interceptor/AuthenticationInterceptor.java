package edu.poly.interceptor;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import edu.poly.model.User;

public class AuthenticationInterceptor implements Interceptor{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void init() {
		// TODO Auto-generated method stub
		
	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("inside auth interceptor");
		@SuppressWarnings("unused")
		Map<String, Object> sessionAttributes = actionInvocation.getInvocationContext().getSession();
		User user = (User) sessionAttributes.get("USER");	
		if (user == null) {
			return Action.LOGIN;
		}else {
			@SuppressWarnings("unused")
			Action action = (Action) actionInvocation.getAction();
			if (action instanceof UserAware) {
				((UserAware) action).setUser(user);
			}
			return actionInvocation.invoke();
		}
	
	}

}
