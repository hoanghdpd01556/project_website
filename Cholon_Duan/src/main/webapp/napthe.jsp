<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styles.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="UTF-8">
</head>
<body>
	<div class="container-fluid navbar-fixed-top">
		<div class="container"
			style="padding-top: 15px; padding-bottom: 15px;">
			<div style="float: left;"></div>
			<div style="float: left">
				<div class="timkiem">
					<form>
						<select name="search" class="corner">
							<option>Tất cả danh mục</option>
							<option>Nhà - Bất động sản</option>
							<option>Huế</option>
						</select> <select name="search" class="corner1">
							<option>Toàn quốc</option>
							<option>Hồ Chí Minh</option>
							<option>Huế</option>
						</select> <input type="text" class="nhaptim" placeholder="Nhập từ khóa" />
						<button class="nhantim">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</form>
				</div>
			</div>
			
			<div style="float:right;">
					<div class="dropdown" style="float:left;">
						<button data-toggle="dropdown" style="background:none;border:none;outline:0;">
						<img src="img/myimg.jpg" width="35" height="35" style="border-radius:50%;">
						<span><s:property value="user.tentaikhoan"/></span>
						<span class="caret"></span></button>
						<div class="dropdown-menu" style="width:250px;">
						<s:url action="napbitcoin" var="urlNap"></s:url>
						<s:url action="lichsugiaodich" var="urlLichsugiaodich"></s:url>
						<s:url action="lichsudangtin" var="urlLichsudangtin"></s:url>
						<s:url action="chinhsuathongtin" var="urlChinhsuathongtin"></s:url>
							<ul class="nonehover">
								<li style="margin-bottom:10px;"><a href="#"><div style="width:50px;height:50px;margin-right:10px;float:left;"><img src="img/myimg.jpg" width="50" height="50" style="border-radius:50%;">
								</div>
								<div><span><s:property value="user.tentaikhoan"/> </span><br><span style="font-size:12px;"><s:property value="user.bitcoint"/> Bitcoin</span></div></a></li>
							</ul>
						  <ul class="memi">
							<a href="<s:property value="#urlNap"/>"><li><img src="img/bitcoin.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Nạp Bitcoin</span></li></a>
							<a href="<s:property value="#urlLichsugiaodich"/>"><li><img src="img/lichsu.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Lịch sử giao dịch</span></li></a>
							<a href="<s:property value="#urlLichsudangtin"/>"><li><img src="img/tin.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Lịch sử đăng tin</span></li></a>
						  </ul>
						  <ul>
						  <a href="<s:property value="#urlChinhsuathongtin"/>"><li><img src="img/setting.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Chỉnh sửa thông tin</span></li></a>
						  <a href="#"><li><img src="img/logout.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Đăng xuất</span></li></a>
						  </ul>
						  </div>
					</div>
					<s:url action="dang-tin" var="urlDangtin"></s:url>
					<a href="<s:property value="#urlDangtin"/>"><button type="button" class="btn1 btndn" style="margin-left:6px;">Đăng tin</button></a>
				</div>
			</div>
		</div>

	</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
			<div class="col-sm-12" style="text-align: center;">
				<div class="background-profile">
					<img src="img/myimg.jpg" width="80" height="80"
						style="border-radius: 50%;">
					<h4>Hồ Triệu long</h4>
					<span>0 Bitcoin</span>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="nav-profile" style="border: 1px solid #D7D9DC;">
					<ul
						style="width: 100%; height: 46px; background-color: #f4f4f4; border-bottom: 1px solid #D7D9DC;">
						<li><a href="<s:property value="#urlLichsudangtin"/>">Lịch sử đăng tin</a></li>
						<li><a href="<s:property value="#urlLichsugiaodich"/>">Lịch sử giao dịch</a></li>
						<li class="active-pro"><a href="<s:property value="#urlNap"/>">Nạp Bitcoin</a></li>
						<li><a href="<s:property value="#urlChinhsuathongtin"/>">Chỉnh sửa thông tin</a></li>
					</ul>
					<div style="background-color: white;">
						<div class="col-sm-12"
							style="padding-left: 15px; padding-top: 15px; background-color: white; padding-bottom: 15px; border-bottom: 1px solid #DDD; text-align: center;">
							<div class="col-sm-4"></div>
							<div class="col-sm-4" style="text-align: center;">
								<s:form action="napbitcoin" theme="simple">
									<div class="form-group">
										<select name="supplier" class="form-control">
											<option>VIETEL</option>
											<option>Nhà - Bất động sản</option>
											<option>Huế</option>
										</select>
									</div>
									<div class="form-group">
										<s:textfield name="seri" class="form-control"
											placeholder="Nhập số Seri" />
									</div>
									<div class="form-group">
										<s:textfield name="pincode" class="form-control"
											placeholder="Nhập mã thẻ cào" />
									</div>
									<div class="form-group">
										<s:submit class="form-control" value="Thanh toán" />
									</div>
								</s:form>
								<s:if test="message != null">
									<s:property value="message" />
								</s:if>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<style>
.nav-profile ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	background-color: #f4f4f4;
}

.nav-profile ul li {
	float: left;
	padding: 12px 12px;
}

.nav-profile ul li a {
	text-decoration: none;
	color: #555;
}

.nav-profile ul li:hover {
	background-color: #f4f4f4;
}

.background-profile {
	background-color: white;
	padding-top: 15px;
	padding-bottom: 15px;
}

.active-pro {
	border-bottom: 1px solid #0879c9;
}

.nav-profile:after {
	clear: both;
	content: ".";
	display: block;
	width: 0px;
	height: 0px;
}
.dropdown-menu  ul a {
	text-decoration: none;
}

.dropdown-menu .nonehover li {
	background-color: white;
}
</style>


</body>
</html>