<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="col-md-2" style="height: 2000px;">
		<div class="profile">
			<a href="#" class="img-profile"><img src="images/user.png"
				class="img-thumbnail" width="60" height="60"></a>
			<div class="profile-name" style="padding-top: 10px;">
				<h4>Long Hoo</h4>
				<small>Leader</small>
			</div>
		</div>
		<div class="navigation">
			<ul>
				<a href="dashboard"><li class="ahere"><span
						class="glyphicon glyphicon-home"></span> Dashboard</li></a>
				<li tabindex="1" data-toggle="collapse" data-parent="#accordion"
					href="#collapseOne" aria-expanded="false"
					aria-controls="collapseOne"><span
					class="glyphicon glyphicon-edit"></span> Posts</li>
				<div class="children-nav">
					<ul id="collapseOne" class="collapse" role="tabpanel"
						aria-labelledby="headingOne">
						<li class="children-nav"><a href="viewPost">View</a></li>
						<li class="children-nav"><a href="addPost">Add</a></li>
					</ul>
				</div>
				<li tabindex="1" class="collapsed" data-toggle="collapse"
					data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
					aria-controls="collapseTwo"><span
					class="glyphicon glyphicon-user"></span> Users</li>
				<div class="children-nav">
					<ul id="collapseTwo" class="collapse" role="tabpanel"
						aria-labelledby="headingTwo">
						<li><a class="children-nav" href="viewUser">View</a></li>
						<li class="children-nav"><a href="addUser">Add</a></li>
					</ul>
				</div>
				<li tabindex="1" class="collapsed" data-toggle="collapse"
					data-parent="#accordion" href="#collapseThree"
					aria-expanded="false" aria-controls="collapseThree"><span
					class="glyphicon glyphicon-user"></span> Quản lý tài khoản</li>
				<div class="children-nav">
					<ul id="collapseThree" class="collapse" role="tabpanel"
						aria-labelledby="headingThree">
						<li><a class="children-nav" href="#">Member</a></li>
						<li class="children-nav"><a href="#">Nhân sự</a></li>
					</ul>
				</div>
				<li tabindex="0"><span class="glyphicon glyphicon-home"></span>
					Dashboard</li>
				<li tabindex="0"><span class="glyphicon glyphicon-home"></span>
					Dashboard</li>
				<li tabindex="1" class="lastli"><span
					class="glyphicon glyphicon-log-out"></span> Đăng xuất</li>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-12"
				style="height: 85px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .1); padding-top: 15px;">
				<div class="here-icon">
					<i class="fa fa-home"> </i>
				</div>
				<div class="here-text">
					<h4>Dashboard</h4>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 15px;">
			<div class="col-md-4">
				<div class="panel">
					<div class="body-panel">
						<div class="panel-icon">
							<i class="fa fa-users"> </i>
						</div>
						<div class="panel-text">
							<h5>NEW USER ACCOUNTS</h5>
							<h1>
							<s:property value="lala"/>
							</h1>
						</div>
					</div>
					<hr>
					<div class="bottom-panel">
						<div class="left-bottom">
							<h5>YESTERDAY</h5>
							<h4>10,009</h4>
						</div>
						<div class="right-bottom">
							<h5>THIS WEEK</h5>
							<h4>10,009</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel" style="background-color: #428bca;">
					<div class="body-panel">
						<div class="panel-icon">
							<i class="fa fa-users"> </i>
						</div>
						<div class="panel-text">
							<h5>NEW USER ACCOUNTS</h5>
							<h1>208,160</h1>
						</div>
					</div>
					<hr>
					<div class="bottom-panel">
						<div class="left-bottom">
							<h5>YESTERDAY</h5>
							<h4>10,009</h4>
						</div>
						<div class="right-bottom">
							<h5>THIS WEEK</h5>
							<h4>10,009</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel" style="background-color: #4e5154;">
					<div class="body-panel">
						<div class="panel-icon">
							<i class="fa fa-users"> </i>
						</div>
						<div class="panel-text">
							<h5>NEW USER ACCOUNTS</h5>
							<h1>208,160</h1>
						</div>
					</div>
					<hr>
					<div class="bottom-panel">
						<div class="left-bottom">
							<h5>YESTERDAY</h5>
							<h4>10,009</h4>
						</div>
						<div class="right-bottom">
							<h5>THIS WEEK</h5>
							<h4>10,009</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6"></div>
			<div class="col-md-6"></div>
		</div>
	</div>
</body>
</html>