<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
	function bs_input_file() {
		$(".input-file")
				.before(
						function() {
							if (!$(this).prev().hasClass('input-ghost')) {
								var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
								element.attr("name", $(this).attr("name"));
								element.change(function() {
									element.next(element).find('input').val(
											(element.val()).split('\\').pop());
								});
								$(this).find("button.btn-choose").click(
										function() {
											element.click();
										});
								$(this).find("button.btn-reset").click(
										function() {
											element.val(null);
											$(this).parents(".input-file")
													.find('input').val('');
										});
								$(this).find('input').css("cursor", "pointer");
								$(this).find('input').mousedown(
										function() {
											$(this).parents('.input-file')
													.prev().click();
											return false;
										});
								return element;
							}
						});
	}
	$(function() {
		bs_input_file();
	});
</script>
<title>Insert title here</title>
</head>
<body>
	<div class="col-md-2" style="height: 100vh;">
		<div class="profile">
			<a href="#" class="img-profile"><img src="images/user.png"
				class="img-thumbnail" width="60" height="60"></a>
			<div class="profile-name" style="padding-top: 12px;">
				<h4>Long Hoo</h4>
				<small>Leader</small>
			</div>
		</div>
		<div class="navigation">
			<ul>
				<a href="dashboard"><li><span class="fa fa-home"></span>Trang
						chủ</li></a>
				<li class="ahere" data-toggle="collapse" data-parent="#accordion"
					href="#collapseOne" aria-expanded="true"
					aria-controls="collapseOne"><span class="fa fa-pencil"></span>Bài
					viết</li>
				<div class="children-nav">
					<ul id="collapseOne" class="collapse show" role="tabpanel"
						aria-labelledby="headingOne">
						<li class="children-nav herehere"><a href="viewPost">Danh
								sách bài viết</a></li>
						<li class="children-nav"><a href="addPost">Đăng bài viết</a></li>
					</ul>
				</div>
				<li tabindex="1" class="collapsed" data-toggle="collapse"
					data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
					aria-controls="collapseTwo"><span class="fa fa-user"></span>Tài
					khoản</li>
				<div class="children-nav">
					<ul id="collapseTwo" class="collapse" role="tabpanel"
						aria-labelledby="headingTwo">
						<li><a class="children-nav" href="viewUser">Danh sách tài
								khoản</a></li>
						<li class="children-nav"><a href="addUser">Thêm mới tài
								khoản</a></li>
					</ul>
				</div>
				<li tabindex="1" class="collapsed" data-toggle="collapse"
					data-parent="#accordion" href="#collapseThree"
					aria-expanded="false" aria-controls="collapseThree"><span
					class="fa fa-buysellads"></span>Quảng cáo</li>
				<div class="children-nav">
					<ul id="collapseThree" class="collapse" role="tabpanel"
						aria-labelledby="headingThree">
						<li><a class="children-nav" href="#">Danh sách quảng cáo</a></li>
						<li class="children-nav"><a href="#">Tạo quảng cáo mới</a></li>
					</ul>
				</div>
				<a href="doanhthu.html"><li><span class="fa fa-bar-chart"></span>Doanh
						thu</li></a>
				<li tabindex="1" class="lastli"><span class="fa fa-sign-out"></span>Đăng
					xuất</li>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-12"
				style="height: 85px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .1); padding-top: 15px;">
				<div class="here-icon">
					<i class="fa fa-user"> </i>
				</div>
				<div class="here-text">
					<h4>Posts</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="padding-top: 15px;">
				<div class="form-group">
					<select class="form-control" id="sel1">
						<option>Tất cả</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
				</div>
			</div>
			<div class="col-md-3" style="padding-top: 15px;">
				<div class="form-group">
					<select class="form-control" id="sel1">
						<option>Toàn quốc</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
				</div>
			</div>
			<div class="col-md-3" style="padding-top: 15px;">
				<button type="button" class="btn btn-primary">Tìm kiếm</button>
			</div>
			<div class="col-md-12" style="padding-top: 15px;">

				<display:table export="true" id="studentTable"
					name=" posts" pagesize="5" class="table table-striped" defaultsort="1" defaultorder="descending"
					cellspacing="5px;" requestURI="">
					<display:column property="id" title="ID" sortable="true"  headerClass="sortable" />
					<display:column property="tentaikhoan" title="Tên tài khoản"
						sortable="true"  headerClass="sortable"/>
					<display:column property="tieude" title="Tiêu đề" sortable="true"  headerClass="sortable"/>
					<display:column property="anh" title="Ảnh" sortable="true"  headerClass="sortable"/>
					<display:column property="noidung" title="Nội dung" sortable="true" headerClass="sortable" />
					<display:column property="categoryType.tendanhmuc" title="Danh mục"
						sortable="true"  headerClass="sortable"/>
					<display:column property="areaType.tenkhuvuc" title="Khu vực"
						sortable="true"  headerClass="sortable"/>
					<display:column property="loaitinType.name" title="Loại tin"
						sortable="true" headerClass="sortable" />
					<display:setProperty name="paging.banner.placement" value="bottom" />
					<display:setProperty name="export.pdf" value="true" />
					<display:setProperty name="export.pdf.filename"
						value="chobitcoint.pdf" />
					<display:setProperty name="export.excel.filename"
						value="chobitcoint.xls" />
						<display:setProperty name="paging.banner.full" value='<div class="pagelinks"><ul class="pagination"><li><a href="{1}">First</a></li><li><a href="{2}">Prev</a></li>{0}<li><a href="{3}">Next</a></li><li><a href="{4}">Last</a></li></ul></span>'/>
						<display:setProperty name="paging.banner.first" value='<ul class="pagination"><li><a>First</a></li> {0}<li><a href="{3}">Next</a></li><li><a href="{4}">Last</a></li></ul>'/>
						<display:setProperty name="paging.banner.last" value='<ul class="pagination"><li><a href="{1}">First</a></li><li><a href="{2}">Prev</a></li>{0}<li><a>Last</a></li></ul>'/>
						<display:setProperty name="paging.banner.page.selected" value='<li class="active"><a>{0}</a></li>'/>
						<display:setProperty name="paging.banner.page.link" value='<li><a href="{1}" title="Go topage {0}">{0}</a></li>'/>
						<display:setProperty name="paging.banner.page.separator" value=''/>
				</display:table>
			</div>
		</div>
	</div>
<style>
.pagebanner{
	display:none;
}
</style>
</body>
</html>