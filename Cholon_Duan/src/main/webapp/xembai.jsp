<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styles.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- JS tạo nút bấm di chuyển trang start -->
        <script src="http://1892.yn.lt/blogger/JQuery/Pagging/js/jquery.twbsPagination.js" type="text/javascript"></script>
<script type="text/javascript">
            $(function () {
                var pageSize = 6; // Hiển thị 6 sản phẩm trên 1 trang
                showPage = function (page) {
                    $(".contentPage").hide();
                    $(".contentPage").each(function (n) {
                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                            $(this).show();
                    });
                }
                showPage(1);
                ///** Cần truyền giá trị vào đây **///
                var totalRows = 40; // Tổng số sản phẩm hiển thị
                var btnPage = 5; // Số nút bấm hiển thị di chuyển trang
                var iTotalPages = Math.ceil(totalRows / pageSize);

                var obj = $('#pagination').twbsPagination({
                    totalPages: iTotalPages,
                    visiblePages: btnPage,
                    onPageClick: function (event, page) {
                        console.info(page);
                        showPage(page);
                    }
                });
                console.info(obj.data());
            });
        </script>
<link href="https://fonts.googleapis.com/css?family=Roboto"
	rel="stylesheet">
<meta charset="UTF-8">
<title>Chợ lớn</title>
<script>
document.getElementsByName(display_column_name)[0].media="none"
		</script>
<style>
.pagebanner {
	display: none;
}

// /** CSS căn id pagination ra giữa màn hình **/
//
#pagination {
	display: flex;
	display: -webkit-flex; /* Safari 8 */
	flex-wrap: wrap;
	-webkit-flex-wrap: wrap; /* Safari 8 */
	justify-content: center;
	-webkit-justify-content: center;
}
</style>
</head>
<body>
	<div class="container-fluid navbar-fixed-top">
		<div class="container"
			style="padding-top: 15px; padding-bottom: 15px;">
			<div style="float: left;"></div>
			<div style="float: left">
				<div class="timkiem">
					<form>
						<select name="search" class="corner">
							<option>Tất cả danh mục</option>
							<option>Nhà - Bất động sản</option>
							<option>Huế</option>
						</select> <select name="search" class="corner1">
							<option>Toàn quốc</option>
							<option>Hồ Chí Minh</option>
							<option>Huế</option>
						</select> <input type="text" class="nhaptim" placeholder="Nhập từ khóa" />
						<button class="nhantim">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</form>
				</div>
			</div>
			<div style="float: right;">
				<button type="button" class="btn1 btndn">Đăng ký</button>
				<button type="button" class="btn1 btndn" style="margin-left: 6px;">Đăng
					nhập</button>
			</div>
		</div>
	</div>
	<div class="container" style="margin-top: 85px;">
		<div class="row">
			<div class="col-sm-8">
				<s:iterator value="posts">
				
				<div class="col-sm-12 contentPage"
					style="padding-left: 0; padding-top: 15px; background-color: white; padding-bottom: 15px; border-bottom: 1px solid #DDD;">
					<div class="col-sm-3">
						<img src="<s:property value="anh"/>" width="100%">
					</div>
					<div class="col-sm-9"
						style="padding-left: 0; padding-top: 0; height: 115px; position: relative;">
						<h4 style="padding-top: 0; margin-top: 0; font-size: 16px;"><s:property value="tieude"/></h4>
						<h4 style="color: #c90927">
							<s:property value="price"/>
							</h5>
							<div class="info-tint" style="right: 0; bottom: 0; position: absolute;">
								<span><s:date name="ngaydang" nice="true" /></s></span> | 
								<span><s:property value="areaType.tenkhuvuc"/></span>
								 <img style="float: right; border-radius: 50%; margin-left: 5px;"
									src="<s:property value="userpost.hinhanh"/>" width="22px" height="22px"> <span
									style="float: right;"><s:property value="userpost.tentaikhoan"/></span>
							</div>
					</div>
				</div>
				</s:iterator>
			<center>	<ul id="pagination"></ul></center>
			</div>

		</div>
	</div>
	<style>
.hidden {
	display: none;
}
</style>
</body>
</html>