<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="col-md-2" style="height:100vh;position:fixed;z-index:9999;top:0;left:0;">	
			<div class="profile">
				<a href="#" class="img-profile"><img src="user.png" class="img-thumbnail" width="60" height="60"></a>
				<div class="profile-name">
					<h4>Long Hoo</h4>
					<small>Leader</small>
				</div>
			</div>
			<div class="navigation">
				<ul>
					<li tabindex="1"><span class="glyphicon glyphicon-home"></span> Dashboard</li>
					<li tabindex="1" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  ><span class="glyphicon glyphicon-user"></span> Posts</li>
						<div class="children-nav">
						<ul id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
							<li><a class="children-nav"><a href="post-view.html">View</a></li>
							<li class="children-nav"><a href="post-add.html">Add</a></li>
						</ul>
						</div>
					<li class="ahere"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span class="glyphicon glyphicon-edit"></span> Users</li>
						<div class="children-nav">
						<ul id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
							<li class="children-nav"><a href="user-view.html">View</a></li>
							<li class="children-nav herehere"><a href="user-add.html">Add</a></li>
						</ul>
						</div>
					<li tabindex="1" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="glyphicon glyphicon-user"></span> Quản lý tài khoản</li>
						<div class="children-nav">
						<ul id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
							<li><a class="children-nav" href="#">Member</a></li>
							<li class="children-nav"><a href="#">Nhân sự</a></li>
						</ul>
						</div>
					<li tabindex="0"><span class="glyphicon glyphicon-home"></span> Dashboard</li>
					<li tabindex="0"><span class="glyphicon glyphicon-home"></span> Dashboard</li>
					<li tabindex="1"><span class="glyphicon glyphicon-log-out"></span> Đăng xuất</li>
				</ul>
			</div>
		</div>
			<div class="col-md-10" style="height:auto;float:right;">
			<div class="row">
				<div class="col-md-12" style="height:85px;box-shadow: 0 2px 2px 0 rgba(0,0,0,.1);padding-top:15px;">
					<div class="here-icon">
						<i class="fa fa-user">
						</i>
					</div>
					<div class="here-text">
						<h4>Users</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="padding-top:15px;">
					<div class="form-body">
						<s:form action="addUser">
							<s:textfield name="user.tentaikhoan" label="Title"></s:textfield>
							<s:textfield name="user.matkhau" label="Mota"/>
							<s:textfield name="user.hoten" label="noidung"></s:textfield>
							<s:textfield name="user.sdt" label="Hinhanh"/>
							<s:textfield name="user.diachi" label="Hinhanh"/>
							<s:textfield name="user.vaitro" label="iddanhmuc"/>
							<s:submit key="Thêm"></s:submit>
							
						</s:form>
						<s:if test="message !=null">
							<hr>
							<s:actionmessage key="message" />
						</s:if>
                    </div>
					
				</div>
			</div>
		</div>
</body>
</html>