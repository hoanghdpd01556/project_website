<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styles.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="UTF-8">
</head>
<body>
	<div class="container-fluid navbar-fixed-top">
		<div class="container"
			style="padding-top: 15px; padding-bottom: 15px;">
			<div style="float: left;"></div>
			<div style="float: left">
				<div class="timkiem">
					<form>
						<select name="search" class="corner">
							<option>Tất cả danh mục</option>
							<option>Nhà - Bất động sản</option>
							<option>Huế</option>
						</select> <select name="search" class="corner1">
							<option>Toàn quốc</option>
							<option>Hồ Chí Minh</option>
							<option>Huế</option>
						</select> <input type="text" class="nhaptim" placeholder="Nhập từ khóa" />
						<button class="nhantim">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</form>
				</div>
			</div>
			<s:if test="user!=null">
				<div style="float: right;">
					<div class="dropdown" style="float: left;">
						<button data-toggle="dropdown"
							style="background: none; border: none; outline: 0;">
							<img src="img/myimg.jpg" width="35" height="35"
								style="border-radius: 50%;"> <span><s:property
									value="user.tentaikhoan" /></span> <span class="caret"></span>
						</button>
						<div class="dropdown-menu" style="width: 250px;">
							<ul class="nonehover">
								<li style="margin-bottom: 10px;"><a href="#"><div
											style="width: 50px; height: 50px; margin-right: 10px; float: left;">
											<img src="img/myimg.jpg" width="50" height="50"
												style="border-radius: 50%;">
										</div>
										<div>
											<span><s:property value="user.tentaikhoan" /> </span><br>
											<span style="font-size: 12px;"><s:property
													value="user.bitcoint" /> Bitcoin</span>
										</div></a></li>
							</ul>
							<ul class="memi">
								<a href="#"><li><img src="img/bitcoin.png" width="30"
										height="30" style="margin-right: 10px;"><span
										style="line-height: 15px;">Nạp Bitcoin</span></li></a>
								<a href="#"><li><img src="img/lichsu.png" width="30"
										height="30" style="margin-right: 10px;"><span
										style="line-height: 15px;">Lịch sử giao dịch</span></li></a>
								<a href="#"><li><img src="img/tin.png" width="30"
										height="30" style="margin-right: 10px;"><span
										style="line-height: 15px;">Lịch sử đăng tin</span></li></a>
							</ul>
							<ul>
								<a href="#"><li><img src="img/setting.png" width="30"
										height="30" style="margin-right: 10px;"><span
										style="line-height: 15px;">Chỉnh sửa thông tin</span></li></a>
								<a href="#"><li><img src="img/logout.png" width="30"
										height="30" style="margin-right: 10px;"><span
										style="line-height: 15px;">Đăng xuất</span></li></a>
							</ul>
						</div>
					</div>
					<s:url action="dang-tin" var="urlDangtin"></s:url>
					<a href="<s:property value="#urlDangtin"/>"><button
							type="button" class="btn1 btndn" style="margin-left: 6px;">Đăng
							tin</button></a>
				</div>
			</s:if>
			<s:else>
				<div style="float: right;">
					<button type="button" class="btn1 btndn">Đăng ký</button>
					<button type="button" class="btn1 btndn" style="margin-left: 6px;">Đăng
						nhập</button>
				</div>
			</s:else>
		</div>

	</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
			<div class="col-sm-12">
				<s:form class="form-horizontal" action="dang-tin" theme="simple">
					<div class="form-group">
						<label class="col-sm-2 control-label">Danh mục</label>
						<div class="col-sm-10">
							<s:select class="form-control" headerKey="0"
								name="areaType.idkhuvuc" list="areatype"
								headerValue="Chon khu vuc" listValue="tenkhuvuc"
								listKey="idkhuvuc" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Tỉnh thành</label>
						<div class="col-sm-10">
							<s:select class="form-control" headerKey="0"
								name="categoryType.iddanhmuc" list="categorytype"
								headerValue="Chon danh muc" listValue="tendanhmuc"
								listKey="iddanhmuc" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Tiêu đề</label>
						<div class="col-sm-10">
							<s:textfield class="form-control" name="post.tieude"
								label="Title" placeholder="Nhập tiêu đề"></s:textfield>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nội dung</label>
						<div class="col-sm-10">
							<s:textarea class="form-control" name="post.noidung"
								label="noidung" rows="10" placeholder="Nhập nội dung"></s:textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Giá bán</label>
						<div class="col-sm-10">
							<input class="form-control" name="post.price" type="text"
								placeholder="Nhập giá bán" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Hình ảnh</label>
						<div class="col-sm-10">
							<s:file class="form-control" name="post.anh" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Loại tin</label>
						<div class="col-sm-10">
							<s:select class="form-control" headerKey="0"
								name="loaitinType.idloaitin" list="loaitintype"
								headerValue="Chọn loại tin" listValue="name" listKey="idloaitin" />
						</div>
					</div>
					<div class="form-group" style="text-align: center;">
						<s:submit class="btn btn-primary" key="Tiếp tục"></s:submit>
					</div>
				</s:form>
			</div>
		</div>
	</div>
	<s:if test="hasActionMessages()">
		<s:actionmessage />
	</s:if>

	<style>
.dropdown-menu  ul a {
	text-decoration: none;
}

.dropdown-menu .nonehover li {
	background-color: white;
}
</style>
</body>
</html>