<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/styles.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta charset="UTF-8">
</head>
<body>
	<div class="container-fluid navbar-fixed-top">
		<div class="container"
			style="padding-top: 15px; padding-bottom: 15px;">
			<div style="float:left;margin-right:30px;">
					<img src="img/long.png" height="35">
				</div>
			<div style="float: left">
				<div class="timkiem">
					<s:form action="search" theme="simple">
						<s:select class="corner" headerKey="0" name="danhmuc" list="categorytype" headerValue="Tất cả danh mục" listValue="tendanhmuc"  listKey="iddanhmuc"/>
						<s:select class="corner1" headerKey="0" name="khuvuc" list="areatype" headerValue="Toàn quốc" listValue="tenkhuvuc" listKey="idkhuvuc"  />
						<s:textfield type="text" name="tukhoa" class="nhaptim" placeholder="Nhập từ khóa" />
						<s:submit type="image" src="img/searchicon.png"  style="padding-left:20px;padding-right:20px;padding-top:10px;padding-bottom:10px;" class="nhantim" id="searchimg">
						</s:submit>
					</s:form>
				</div>
			</div>
			<s:if test="user!=null">
				<div style="float:right;">
					<div class="dropdown" style="float:left;">
						<button data-toggle="dropdown" style="background:none;border:none;outline:0;">
						<img src="img/myimg.jpg" width="35" height="35" style="border-radius:50%;">
						<span><s:property value="user.tentaikhoan"/></span>
						<span class="caret"></span></button>
						<div class="dropdown-menu" style="width:250px;">
						<s:url action="napbitcoin" var="urlNap"></s:url>
						<s:url action="lichsugiaodich" var="urlLichsugiaodich"></s:url>
						<s:url action="lichsudangtin" var="urlLichsudangtin"></s:url>
						<s:url action="chinhsuathongtin" var="urlChinhsuathongtin"></s:url>
						<s:url action="logout" var="urldangxuat"></s:url>
							<ul class="nonehover">
								<li style="margin-bottom:10px;"><a href="#"><div style="width:50px;height:50px;margin-right:10px;float:left;"><img src="img/myimg.jpg" width="50" height="50" style="border-radius:50%;">
								</div>
								<div><span><s:property value="user.tentaikhoan"/> </span><br><span style="font-size:12px;"><s:property value="user.bitcoint"/> Bitcoin</span></div></a></li>
							</ul>
						  <ul class="memi">
							<a href="<s:property value="#urlNap"/>"><li><img src="img/bitcoin.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Nạp Bitcoin</span></li></a>
							<a href="<s:property value="#urlLichsugiaodich"/>"><li><img src="img/lichsu.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Lịch sử giao dịch</span></li></a>
							<a href="<s:property value="#urlLichsudangtin"/>"><li><img src="img/tin.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Lịch sử đăng tin</span></li></a>
						  </ul>
						  <ul>
						  <a href="<s:property value="#urlChinhsuathongtin"/>"><li><img src="img/setting.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Chỉnh sửa thông tin</span></li></a>
						  <a href="<s:property value="#urldangxuat"/>"><li><img src="img/logout.png" width="30" height="30" style="margin-right:10px;"><span style="line-height:15px;">Đăng xuất</span></li></a>
						  </ul>
						  </div>
					</div>
					<s:url action="dang-tin" var="urlDangtin"></s:url>
					<a href="<s:property value="#urlDangtin"/>"><button type="button" class="btn1 btndn" style="margin-left:6px;">Đăng tin</button></a>
				</div>
			</s:if>
			<s:else>
				<div style="float: right;">
				<s:url action="dangnhap" var="urlDangnhap"></s:url>
				<s:url action="dangky" var="urlDangky"></s:url>
				<a href="<s:property value="#urlDangky"/>"><button type="button" class="btn1 btndn">Đăng ký</button></a>
					<a href="<s:property value="#urlDangnhap"/>"><button type="button" class="btn1 btndn" style="margin-left: 6px;">Đăng nhập</button></a>
				</div>
			</s:else>
		</div>

	</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
			<div class="col-sm-6">
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/house.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/house1.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/house2.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/house3.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Nhà - Bất động sản</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/bep2.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/bep1.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/bep3.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/bep4.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Đồ bếp</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/giay1.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/ao.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/dongho.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/du.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Thời trang - Đồ dùng cá nhân</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/xehoi.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/moto.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/xedap.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/xedap1.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Ô tô - Xe máy - Xe đạp</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/anh1.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/anh2.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/dientu1.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/dientu2.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Đồ điện tử</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
				<div class="danhmuc">
					<div class="imgbig">
						<img src="img/anh1.jpg" class="img-responsive" width="100%">
					</div>
					<div class="imgsmall">
						<div class="imgsmall1">
							<img src="img/anh2.jpg" class="img-responsive">
						</div>
						<div class="imgsmall2">
							<img src="img/anh2.jpg" class="img-responsive">
						</div>
						<div class="imgsmall3">
							<img src="img/anh2.jpg" class="img-responsive">
						</div>
					</div>
					<div class="danhmuctitle">
						<h3>Dịch vụ</h3>
						<span>222 bài đăng mới</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
	.dropdown-menu  ul a{
	text-decoration:none;
	
}
.dropdown-menu .nonehover li{
background-color:white;
}


	</style>
</body>
</html>